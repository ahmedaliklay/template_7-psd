$(function(){
    new WOW().init();
    var typed = new Typed('.typed', {
        strings: ["First sentence.",'Welcome to my profile Text Generator','Move and rotate elements by dragging them.','Resize by dragging this corner. '],
        typeSpeed: 60,
        showCursor: false,
        backDelay: 1000,
        loop: true ,
    }); 
});
